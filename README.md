# Claim Definition Policy Shifting

This VBA code was used to adjust large datasets to reflect the newest indemnity claim definition change. The module shifts the affected loss history section along with blanking some UW inputs to isolate change. 

The new dataset was then run through the pricing algorithm and compared to unshifted set to determine impact. 
